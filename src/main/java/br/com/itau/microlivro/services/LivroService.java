package br.com.itau.microlivro.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.microlivro.models.Livro;
import br.com.itau.microlivro.repositories.LivroRepository;
import br.com.itau.microlivro.viewobjects.Autor;

@Service
public class LivroService {

	@Autowired
	LivroRepository livroRepository;

	@Autowired
	AutorService autorService;

	public boolean atualizarProprietario(int idLivro, int idUsuarioProprietario) {
		
		Optional<Livro> livro = obterLivroPorId(idLivro);

		if (!livro.isPresent()) {
			return false;
		} else {
			livro.get().setIdUsuario(idUsuarioProprietario);
			livroRepository.save(livro.get());
			return true;
		}

	}

	public List<Livro> buscaPorAutor(String nome) {
		Optional<Autor> autor = autorService.obterautor(nome);
		if (autor.isPresent()) {
			return livroRepository.findAllByIdAutor(autor.get().getId());
		}
		return Collections.emptyList();
	}

	public Iterable<Livro> buscarTodos() {
		return livroRepository.findAll();
	}

	public Optional<Livro> obterLivroPorId(int id) {
		return livroRepository.findById(id);
	}

	public Iterable<Livro> obterLivroPorTitulo(String titulo) {
		return livroRepository.findAllByTitulo(titulo);
	}

	public boolean inserir(Livro livros) {
		livroRepository.save(livros);
		return true;
	}

	public boolean alterar(int id, Livro livro) {

		Optional<Livro> livroOptional = livroRepository.findById(id);

		if (livroOptional.isPresent()) {
			livro = mesclarAtributos(livro, livroOptional.get());
			livroRepository.save(livro);
			return true;
		}

		return false;
	}

	private Livro mesclarAtributos(Livro novo, Livro antigo) {
		if (novo.getTitulo() != null && !novo.getTitulo().isEmpty()) {
			antigo.setTitulo(novo.getTitulo());
		}

		// if (novo.getAutor() != null) {
		/// antigo.setAutor(novo.getAutor());
		// }

		if (novo.getCategoria() != null) {
			antigo.setCategoria(novo.getCategoria());
		}

		antigo.setDisponivel(1);

		return antigo;
	}

	public boolean deletarLivro(int id) {
		Optional<Livro> livroOptional = obterLivroPorId(id);

		if (livroOptional.isPresent()) {
			livroRepository.delete(livroOptional.get());
			return true;
		}
		return false;
	}

}