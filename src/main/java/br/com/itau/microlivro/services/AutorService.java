package br.com.itau.microlivro.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.microlivro.clients.AutorClient;
import br.com.itau.microlivro.viewobjects.Autor;

@Service
public class AutorService {

	@Autowired
	AutorClient autorClient;

	public Optional<Autor> obterautor(String nome) {
		List<Autor> ListaAutores = autorClient.buscarAutores();
		for (Autor autor : ListaAutores) {
			if (autor.getNome().equals(nome)) {
				return Optional.of(autor);
			}
		}
		return Optional.empty();
	}
}
