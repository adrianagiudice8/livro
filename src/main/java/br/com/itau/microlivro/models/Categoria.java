package br.com.itau.microlivro.models;

public enum Categoria {
	ACAO("Ação"),
	AVENTURA("Aventura"),
	COMEDIA("Comédia"),
	FICCAO("Ficção"),
	INFANTIL("Infantil"),
	ROMANCE("Romance"),
	TERROR("Terror");
	
	private String nome;

	private Categoria(String nome) {
		this.nome = nome;
	}

	public String toString() {
		return nome;
	}
}
