package br.com.itau.microlivro.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.itau.microlivro.models.Livro;

public interface LivroRepository extends CrudRepository<Livro, Integer> {

	@Query(value = "SELECT * FROM trocalivros.livros WHERE titulo like CONCAT(:titulo,'%')", nativeQuery = true)
	public Iterable<Livro> findAllByTitulo(@Param("titulo") String titulo);
	
	public List<Livro> findAllByIdAutor(int idAutor);

}